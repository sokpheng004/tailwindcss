function clickMe() {
  const body = document.getElementById("body");
  alert(body.style.backgroundColor);
}

let html = "";
function generateCard() {
  const cardSection = document.getElementById("card");
  for (let i = 0; i < 5; i++) {
    html =
      html +
      `<div class="w-72 hover:shadow-lg hover:shadow-blue-50 hover:cursor-pointer border rounded-md h-52 p-2">
            <h1 class="font-semibold">CSTAD</h1>
            <hr />
          </div>`;
  }
  cardSection.innerHTML = html;
}
let event_ = "";

function generateEventCard() {
  const eventCardSection = document.getElementById("event");
  const eventInfo = [
    {
      eventName: "Cambodia Hackaton",
      date: "02-January-2024",
      imgUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi-UnBgT3FakEEtpsnb37ZwdMKZgoa9AVO0IWXKtsbyw&s",
    },
    {
      eventName: "CSTAD - Coding",
      date: "02-March-2023",
      imgUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0FwzzWZpjjkaS7kbVZ0KBczcvGBCAvixB4tcD5NezQQ&s",
    },
    {
      eventName: "CSTAD - CTF",
      date: "02-June-2024",
      imgUrl:
        "https://cloudfront-us-east-2.images.arcpublishing.com/reuters/CUXAHHXCLRNX7AIU5APRCKWY4A.jpg",
    },
  ];
  eventInfo.map((e) => {
    event_ =
      event_ +
      `              <div class="mt-2 flex gap-2 hover:cursor-pointer">
                <img
                  src=${e.imgUrl}
                  alt="image"
                  class="rounded-full w-10 h-10 object-cover border-2 border-gray-200"
                />
                <!-- name of event -->
                <div class="flex flex-col">
                  <h1 class="font-medium">${e.eventName}</h1>
                  <p class="text-sm text-gray-400 -mt-1 hover:text-blue-400 duration-200 hover:underline">${
                    e.date ? e.date : "02-March-2024"
                  }</p>
                </div>
              </div>`;
  });
  eventCardSection.innerHTML = event_;
}

generateEventCard();

function generatePostEventCard() {
  let postHtml = "";
  const postEventCardSection = document.getElementById("post");
  const postEventInfo = [
    {
      postedDate: "",
      account: {
        accountName: "Kim Chansokpheng",
        accountImage:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzwWtCyoG3iMwLf93vKExDI0qvXam1CvPNMM1wSs6nDw&s",
      },
      postContent: "I love hacking things, and hacking caree. ❤️❤️❤️",
      imgUrl: "https://cdn.metatime.com/landing/blog/1686575019blobid0.jpg",
    },
    {
      account: {
        accountName: "Ing MuyLeang",
        accountImage:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzwWtCyoG3iMwLf93vKExDI0qvXam1CvPNMM1wSs6nDw&s",
      },
      postContent: "Coding is my life",
      imgUrl:
        "https://miro.medium.com/v2/resize:fit:10116/1*VBAaUoOm0WRmAgxXVUwHIA.jpeg",
    },
    {
      account: {
        accountName: "K.O",
        accountImage:
          "https://img.freepik.com/free-photo/handsome-bearded-guy-posing-against-white-wall_273609-20597.jpg?size=626&ext=jpg&ga=GA1.1.553209589.1714003200&semt=sph",
      },
      postContent: "Life is challenge",
      imgUrl: "https://i.ytimg.com/vi/47vmgMhsAdw/maxresdefault.jpg",
    },
  ];
  postEventInfo.map((e) => {
    postHtml =
      postHtml +
      `
                  <div class="w-full mt-2">
                <!-- image -->
                <div class="md:h-72">
                  <img
                    src=${e.imgUrl}
                    alt="404"
                    class="w-full h-full object-cover rounded-lg"
                  />
                </div>
                <!-- account, content and like -->
                <div class="w-full">
                  <div class="mt-2 flex gap-2 hover:cursor-pointer">
                    <img
                      src=${e.account.accountImage}
                      alt="image"
                      class="rounded-full w-10 h-10 object-cover border-2 border-gray-200"
                    />
                    <!-- name of event -->
                    <div class="flex flex-col">
                      <h1 class="font-medium">${e.account.accountName}</h1>
                      <p
                        class="text-sm text-gray-400 -mt-1  duration-300 hover:underline"
                      >
                        ${e.postedDate ? e.postedDate : "26-April-2024"}
                      </p>
                    </div>
                  </div>
                  <!-- content -->
                  <div class="mt-2">
                    <p class="">
                      ${
                        e.postContent
                          ? e.postContent
                          : "I love hacking things, and hacking caree. ❤️❤️❤️"
                      }
                    </p>
                  </div>
                  <!-- like -->
                </div>
                <!-- break -->
                <hr class="mt-2" />
                <!-- reaction -->
                <div class="flex gap-4 mt-2 text-sm">
                  <button class="border p-2 rounded-md">Like</button>
                  <button class="border p-2 rounded-md">Comment</button>
                  <button class="border p-2 rounded-md">Share</button>
                </div>
                <hr class="mt-2"/>
              </div>
    `;
  });
  postEventCardSection.innerHTML = postHtml;
}
generatePostEventCard();

// loading;
function loading() {
  const loading = document.getElementById("loading");
  loading.innerHTML = `<div class="loader">
  <div class="box box-1">
    <div class="side-left"></div>
    <div class="side-right"></div>
    <div class="side-top"></div>
  </div>
  <div class="box box-2">
    <div class="side-left"></div>
    <div class="side-right"></div>
    <div class="side-top"></div>
  </div>
  <div class="box box-3">
    <div class="side-left"></div>
    <div class="side-right"></div>
    <div class="side-top"></div>
  </div>
  <div class="box box-4">
    <div class="side-left"></div>
    <div class="side-right"></div>
    <div class="side-top"></div>
  </div>
</div>`;
}
window.onload = function () {
  loading();
};
document.addEventListener("DOMContentLoaded", function () {
  console.log("HTML structure loaded!");
  // Your code to be executed after HTML structure is ready
  loading();
});
const loadingV = document.getElementById("loading");
loadingV.style.display = "none";
